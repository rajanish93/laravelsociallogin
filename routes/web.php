<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/player-list', 'PlayerController@playerList')->name('player-list');

//Social login Route

Route::get('login/{provider}', 'LoginController@redirectToProvider');
Route::get('login/{provider}/callback','LoginController@handleProviderCallback');
