@extends('layouts.app')

@section('content')
<div class="container">
<table id="example" class="table">
  <thead>
    <tr>
      <th scope="col">Player Full Name</th>
      <th scope="col"> Player Team Full Name</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($playerData as $player)
        <tr>
          <td>{{$player->first_name." " .$player->last_name}}</td>
          <td>{{$player->team->full_name}}</td>
        </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection

@section('scripts')

 <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable({
        "searching": false,
        "ordering": false,
        "lengthChange": false,
        "info": false,
        "iDisplayLength": 5
    });
} );
</script>
@endsection


